#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>


int main()
{
    std::vector<int> population {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::vector<int> sample;
    std::sample(population.begin(), population.end(), 
                std::back_inserter(sample),
                5,
                std::mt19937{std::random_device{}()});

    for(int i: sample)
        std::cout << i << " "; //prints 5 randomly chosen values from population vector
    std::cout << " -------- " << std::endl; //prints 5 randomly chosen values from population vector        

    std::sample(population.begin(), population.end(), 
            std::back_inserter(sample),
            5,
            std::mt19937{std::random_device{}()});

    for(int i: sample)
        std::cout << i << " "; //prints 5 randomly chosen values from population vector
}